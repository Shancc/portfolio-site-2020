import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  artworks: [
    {
      id: 1,
      name: "Leader board",
      subTitle: "Design and Develop client / hub size leader board for students",
      breif:
        "Design and Develop client / hub size leader board for students",
      solution:
        "Created leader board for students based on their classroom prefermence includes test resultes, attendence, and assignements",
      indursty: "Education",
      skills: ["Vue.js", "Vuex", "LESS", "MySQL", "svg", "Sketch", "Responsive"],
      scheme: ["#F95400", "#1EB482", "#EBEEF3"],
      heroImg: "../../public/portfolio/AskVideo-LeaderBoardDesktop.jpg",
      imgs: ["../../public/portfolio/AskVideo-LeaderBoardDesktop.jpg", "https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any"],
      href: "google.com"
    },
    {
      id: 2,
      name: "Coastal Peoples Gallery",
      breif:
        "Client wants to update their exisiting website to prefection the user expierance and increase online exporsure.",
      solution: "Present Client with two CMS system and set up their system",
      indursty: "art",
      skills: ["Sketch", "Online Identity", "Branding", "Wordpress"],
      scheme: ["#2e4d6a", "white", "black"],
      heroImg: "https://placeimg.com/640/480/any",
      imgs: ["https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any"],
      href: "google.com"
    },
    {
      id: 3,
      name: "Fubon Bank",
      breif:
        "Client wants to update their exisiting website to prefection the user expierance and increase online exporsure.",
      solution: "Present Client with two CMS system and set up their system",
      indursty: "bank",
      skills: [
        "Adobe Indesign",
        "Adobe Photoshop",
        "Branding",
        "Front-end web development"
      ],
      scheme: ["#2e4d6a", "white", "black"],
      heroImg: "https://placeimg.com/640/480/any",
      imgs: ["https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any"],
      href: "google.com"
    },
    {
      id: 4,
      name: "Open Door Group",
      breif:
        "Client wants to update their exisiting website to prefection the user expierance and increase online exporsure.",
      solution: "Present Client with two CMS system and set up their system",
      indursty: "bank",
      skills: ["Sketch", "Online Identity", "Branding", "Wordpress"],
      scheme: ["#2e4d6a", "white", "black"],
      heroImg: "https://placeimg.com/640/480/any",
      imgs: ["https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any"],
      href: "google.com",
    },
    {
      id: 5,
      name: "libang Paint Champing site",
      breif:
        "Client wants to update their exisiting website to prefection the user expierance and increase online exporsure.",
      solution: "Present Client with two CMS system and set up their system",
      indursty: "bank",
      skills: ["Sketch", "Online Identity", "Branding", "Wordpress"],
      scheme: ["#2e4d6a", "white", "black"],
      heroImg: "https://placeimg.com/640/480/any",
      imgs: ["https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any", "https://placeimg.com/640/480/any"],
      href: "google.com"

    }
  ],

};
const getters = {
  getPortfoioByID: (state) => (id) => {
    return state.artworks.find(artwork => artwork.id === id);
  },

  getTodoById: (state, getters) => (id) => {
    return getters.artworks.find(artwork => artwork.id === id)
  },


};

export default new Vuex.Store({
  state,
  getters
})


