import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import VueX from "vuex";
import store from './store'
import { routes } from "./routes";
import VueKinesis from 'vue-kinesis';
import VueMeta from 'vue-meta';
// import VueParallaxJs from 'vue-parallax-js'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false;

Vue.use(VueRouter, VueKinesis, VueX);
Vue.use(VueMeta);

const router = new VueRouter({
  routes,
  mode: "history"
});

new Vue({
  render: h => h(App),
  store,
  router
}).$mount("#app");
