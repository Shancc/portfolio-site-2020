import Home from "./../components/Home.vue";
import About from "./../components/About.vue";
import Gallery from "./../components/Gallery.vue";
import Portfolio from "./../components/Portfolios/Portfolio.vue";

export const routes = [
  {
    path: "",
    component: Home,
    name: "homepage",
    meta: {
      auth: false,
      title: 'YuShan Chang'
    }
  },
  {
    path: "/resume",
    component: About,
    name: "resume",
    meta: {
      auth: false,
      title: 'resume '
    }
  },
  {
    path: "/work",
    component: Gallery,
    name: "work",
    meta: {
      auth: false,
      title: 'Works '
    }
  },
  {
    path: "/portfolio/:id",
    component: Portfolio,
    name: "portfolioArt",
    props: true,
  }

];
